### **_fresh_ - a new [mk-refresh](https://bitbucket.org/michaelkors/mk-refresh)**

* * *

~~check out [the documentation](https://mk-confluence.sparkred.com/display/FRSH) for this repo~~ the docs are under construction

* * *

**getting started**:

1. clone this repository with sourcetree or do it directly from your terminal:

    `git clone https://brianmk@bitbucket.org/brianmk/fresh.git && cd fresh`

2. install the dependencies with [yarn](https://code.facebook.com/posts/1840075619545360):

    `sudo npm install -g yarn && sudo yarn` (type in your password if prompted to)

3. install the [command line tool](https://bitbucket.org/brianmk/fresh-cli):

    `sudo npm install -g git+https://brianmk@bitbucket.org/brianmk/fresh-cli.git`

4. run `fresh` to develop with the locale en_US (*read below if you get any errors at this step*)

    * develop with any other locale with `fresh --locale language_REGION`, or the shorthand version `fresh -l language_REGION`

        ex: fr_CA (French x Canada) - run `fresh -l fr_CA`

* * *

frequently encountered problems on step 4:

* if you get an error like `Error: EACCES: permission denied`, run `sudo chmod -R 777 . && sudo chmod -R 777 ~/.npm`

* if you get an error like `PhantomJS not found on PATH`, run `sudo npm install phantomjs-prebuilt@2.1.8`

* if you get an error like `ENOENT: no such file or directory, scandir '/.../node_modules/node-sass/vendor'`, run `sudo npm install -g node-sass && sudo npm rebuild node-sass --force`

* if you get an error like `SyntaxError: Block-scoped declarations (let, const, function, class) not yet supported outside strict mode`, you're running an outdated version of node. update it to the latest version at https://nodejs.org/en/ (green box on the right). at the time this was typed, the version is 7.10.0