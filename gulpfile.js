// indentation
const origConsoleLog = console.log
    , inspect = require('util').inspect

console.log = (...logs) => logs.map(log => origConsoleLog(`  ${log}`))
console.inspect = log => console.log(inspect(log,false,null))

require('require-dir')('./app/core/config/gulp', { recurse: true })
