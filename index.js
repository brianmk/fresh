const express = require('express')
    , app = express()
    , bodyParser = require('body-parser')
    , path = require('path')
    , port = process.env.PORT || require('./app/core/config/settings').nodemon.port.webPort
    , inspect = require('util').inspect

console.inspect = log => console.log(inspect(log,false,null))


// start the server
require('./app/core/config/routes')(app)

app.use(bodyParser.urlencoded({ extended: true}))
app.use(bodyParser.json())
// app.use(express.compress())
app.use('/', express.static(path.join(__dirname, 'dist')))

app.use((req,res,next) => res.render(`core/containers/error/error.ejs`, {
  page: 'undefined',
  locale: {
    lang: 'undefined',
    region: 'UNDEFINED'
  }
}))

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, './app'))

app.listen(port)
