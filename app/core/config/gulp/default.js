'use strict';

const gulp = require('gulp')
  , env = process.env.NODE_ENV || 'development'

gulp.task('default', ['clean'], () => gulp.start(env))
