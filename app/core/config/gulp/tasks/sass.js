const gulp = require('gulp')
  , settings = require('../../settings')
  , paths = settings.paths
  , plugins = require('gulp-load-plugins')()
  , browserSync = require('browser-sync').get('Fresh BrowserSync Server')


gulp.task('sass', () =>
  gulp.src(paths.sass.src)
    // .pipe(plugins.sourcemaps.init()) // this is crashing compilation @ "&picture, &picture-container, &legal, &caption { @include modifiers(){"
    .pipe(plugins.sass(settings.sass).on('error', plugins.sass.logError))
    .pipe(plugins.autoprefixer({ browsers: ['last 2 version'] }))
    .pipe(plugins.concat('style.css'))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(plugins.sourcemaps.write())
    .pipe(gulp.dest(paths.sass.dest))
    .pipe(browserSync.reload({stream: true}))
)
