const gulp = require('gulp')
	, paths = require('../../settings').paths
	, gulpLoadPlugins = require('gulp-load-plugins')
	, plugins = gulpLoadPlugins()
	, browserSync = require('browser-sync').get('Fresh BrowserSync Server')

gulp.task('fonts', () =>
  gulp.src(paths.fonts.dev)
  	.pipe(gulp.dest(paths.fonts.dest))
  	.pipe(browserSync.reload({stream:true}))
)
