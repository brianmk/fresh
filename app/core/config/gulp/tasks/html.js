const gulp = require('gulp')
  , paths = require('../../settings').paths
	, browserSync = require('browser-sync').get('Fresh BrowserSync Server')

gulp.task('html', () =>
  gulp.src(paths.html.src)
		.pipe(browserSync.reload({stream:true}))
)
