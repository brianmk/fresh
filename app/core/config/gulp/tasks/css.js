const gulp = require('gulp')
	, paths = require('../../settings').paths
	, gulpLoadPlugins = require('gulp-load-plugins')
	, plugins = gulpLoadPlugins()
	, browserSync = require('browser-sync').get('Fresh BrowserSync Server')
	, sassLint = require('gulp-sass-lint')

gulp.task('css', () => {
  gulp.src(paths.css.dev)
    .pipe(gulp.dest(paths.css.dest))
    .pipe(browserSync.reload({stream:true}))

	return gulp.src(paths.css.src)
    .pipe(plugins.concat('style.css'))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(gulp.dest(paths.css.dest))
    .pipe(browserSync.reload({stream:true}))
})

gulp.task('css-optimize', () =>
  gulp.src(`${paths.css.dest}/style.min.css`)
		.pipe(plugins.csslint('.csslintrc'))
		.pipe(plugins.cssmin())
		.pipe(gulp.dest(paths.css.dest))
)
