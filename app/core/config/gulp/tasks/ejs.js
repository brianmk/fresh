const gulp = require('gulp')
    , plugins = require('gulp-load-plugins')()
    , browserSync = require('browser-sync').get('Fresh BrowserSync Server')
    , strip = require('gulp-strip-comments')
    , paths = require('../../settings').paths
    , {Provider, getRouteArgs, CustomSassCompiler} = require('../../../utilities/_utilities')

    // loops through a language_REGION's pages property
    , page_looper = lpd => callback => {
        for(page in lpd.pages) CustomSassCompiler(lpd)(page, callback(page))
      }

    // ejs renderer through which the gulp.src will be piped
    , gulp_ejs = (localeData, components) => (page, production) => customStyles => {

        // if(page === 'collectionep') console.inspect(localeData.destinationKors.extra)

        return plugins.ejs(getRouteArgs(localeData, components)(production, page)(customStyles), {ext:'.html'})
      }

    , {localeData, components} = Provider()
    , gejs = gulp_ejs(localeData, components) // gulp_ejs with components defined
    , pl = page_looper(localeData)


// renders the html for express during dev
gulp.task('ejs', () => pl(page => css => {
  gulp
    .src(paths.ejs.containers.page)
    .pipe(gejs(page, false)(css))
    .pipe(browserSync.reload({stream:true}))
}))

// compiles ejs into html files, whose code is most likely to be uploaded to content slots
gulp.task('build-ejs', () => pl(page => css =>
  gulp
    .src(paths.ejs.containers.page)
    .pipe(gejs(page, true)(css))
    .pipe(plugins.rename({
      prefix: `${localeData.lang}_${localeData.region}-`,
      basename: page
      // , suffix: `-${localeData.lang}_${localeData.region}`
    }))
    .pipe(strip())
    .pipe(gulp.dest(paths.productionPath.html))
))
