const gulp = require('gulp')
  , del = require('del')

gulp.task('clean', cb => del(['./dist/**/*', './dist/assets', './dist/*.html'], cb))

gulp.task('cleanProd', cb => del(['./production'], cb))