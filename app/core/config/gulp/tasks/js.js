const gulp = require('gulp')
  , paths = require('../../settings').paths
  , gulpLoadPlugins = require('gulp-load-plugins')
  , plugins = gulpLoadPlugins()
  , browserSync = require('browser-sync').get('Fresh BrowserSync Server')
  , babel = require('gulp-babel')

gulp.task('js', () =>
  gulp.src(paths.js.src)
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(plugins.uglify())
  	.pipe(plugins.concat('site.js'))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(gulp.dest(paths.js.dest))
    .pipe(browserSync.reload({stream:true}))
)

gulp.task('jshint', () =>
  gulp.src(paths.js.src)
    .pipe(plugins.jshint('.jshintrc'))
    .pipe(plugins.jshint.reporter())
)

gulp.task('js-optimize', () =>
  gulp.src(paths.js.dest+'/site.min.js')
    .pipe(plugins.jshint('.jshintrc'))
    .pipe(gulp.dest(paths.js.dest))
)
