const gulp = require('gulp')
  , gulpLoadPlugins = require('gulp-load-plugins')
  , settings = require('../../settings')
  , nodemon = require('nodemon')
  , {BrowserSyncer} = require('../../../utilities/_utilities')


let started = false
gulp.task('devServe', cb =>
  nodemon(settings.nodemon.settings)
    .on('start', () => {
      // to avoid nodemon being started multiple times
      if(!started) {
        cb()
        started = true
      }
    })
)