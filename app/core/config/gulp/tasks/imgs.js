const gulp = require('gulp')
  , gulpLoadPlugins = require('gulp-load-plugins')
  , plugins = gulpLoadPlugins()
  , browserSync = require('browser-sync').get('Fresh BrowserSync Server')
  , paths = require('../../settings').paths
  , genv = plugins.util.env
  , [lang, region] = genv.hasOwnProperty('locale') && genv.locale.length
      ? genv.locale.split('_')
      : [
          genv.lang ? genv.lang.toLowerCase() : 'en',
          genv.region ? genv.region.toUpperCase() : 'US'
        ]


gulp.task('imgs', done => {

  // copy the imgs of each of the locale's pages into their respective folders in dist
  paths.imgs.i18n(`${lang}_${region}`)
    .map((imgs, index, imgsArr) => {
      gulp.src(imgs.src)
        .pipe(gulp.dest(imgs.dest))
        // .pipe(browserSync.reload({stream:true})) // not all folders will be copied (@TODO: find out why)
    })

  // copy global imgs
  return gulp.src(paths.imgs.src.concat(paths.imgs.dev))
    .pipe(gulp.dest(paths.imgs.dest))
    .pipe(browserSync.reload({stream:true}))

})

gulp.task('imgs-optimize', () => {
  return gulp.src(paths.imgs.src)
		.pipe(plugins.imagemin({optimizationLevel: 8}))
		.pipe(gulp.dest(paths.imgs.dest))
})
