'use strict';

const gulp = require('gulp')
    , plugins = require('gulp-load-plugins')()
    , paths = require('../settings').paths
    , genv = plugins.util.env
    , [lang, region] = genv.hasOwnProperty('locale') && genv.locale.length
        ? genv.locale.split('_')
        : [
            genv.lang ? genv.lang.toLowerCase() : 'en',
            genv.region ? genv.region.toUpperCase() : 'US'
          ]


gulp.task('production', [
	'css-optimize'
  , 'js-optimize'
	, 'imgs-optimize'
], () => gulp.start('prod-copy'))



gulp.task('prod-copy', ['cleanProd'], () => {

  // copy - js
  gulp.src(paths.js.dest + '/*.*')
    .pipe(gulp.dest(paths.productionPath.js))
    .pipe(plugins.filesize())

  // first build the .html files so they can be piped into uncss (remove unused styles)
  gulp.start('build-ejs', () => {

    // copy - css
    gulp.src(paths.sass.dest + '/style.min.css') // only copy style.min.css
      // .pipe(plugins.uncss({ // remove unused styles (maybe we shouldnt use this because some class names are generated dynamically)
      //   html: [`${paths.productionPath.html}/*.html`]
      // }))
      .pipe(gulp.dest(paths.productionPath.css))
      .pipe(plugins.filesize())

  })

})
