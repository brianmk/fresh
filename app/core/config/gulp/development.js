const gulp = require('gulp')
  , plugins = require('gulp-load-plugins')()
  , genv = plugins.util.env
  , [lang, region] = genv.hasOwnProperty('locale') && genv.locale.length
      ? genv.locale.split('_')
      : [
          genv.lang ? genv.lang.toLowerCase() : 'en',
          genv.region ? genv.region.toUpperCase() : 'US'
        ]
  , jsonfile = require('jsonfile')
  , browserSync = require('browser-sync')
  , open = require('open')
  , emoji = require('node-emoji')
  , webPort = require('../settings').nodemon.port.webPort
  , {BrowserSyncer, WiFiDetector, SapientCssCacher} = require('../../utilities/_utilities')




gulp.task('locale', () => jsonfile.writeFile(
  'dist/locale.lock', {lang, region}, err =>
  !genv.silent ? console.log(err ? err : `${emoji.get('floppy_disk')}  File 'locale.lock' saved with data: ${lang}_${region}`) : ''
))

gulp.task('development', [
  'fonts',
  'sass',
  'css',
  'js',
  'imgs',
  'html',
  'locale' // this must be completed before serving in order to let ejs know which locale is being used
], () => {

  WiFiDetector(detected => {

    gulp.task('serve', [ // to be run after task 'locale'
      'ejs',
      'devServe',
      'browserSync'
    ], () => { // open in browser once serving is complete
      if(detected){ // if on MK0rsData network, open localhost.kors.local
        SapientCssCacher(() => {
          console.log(`${emoji.get('rocket')}  Opening 'localhost.kors.local:${webPort}'`)
          open(`http://localhost.kors.local:${webPort}`)
        })
      } else { // otherwise just open localhost
        console.log(`${emoji.get('rocket')}  Opening 'localhost:${webPort}'`)
        open(`http://localhost:${webPort}`)
      }
    })
    gulp.start('serve')
  })

  gulp.task('browserSync', () => BrowserSyncer())

})