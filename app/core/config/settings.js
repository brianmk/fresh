'use strict'

const fs = require('fs')
		, path = require('path')
		, getDirectories = srcpath => fs.readdirSync(srcpath).filter(file => fs.statSync(path.join(srcpath, file)).isDirectory())

		, app								= './app/'
		, appAssets    			= `${app}assets/`
		, appCore      			= `${app}core/`
		, dist   						= './dist/'
		, distAssets 			  = `${dist}assets/`
		, production   			= './production/'
		, clientNameSpace 	= 'mkors'

		, webPort         	= 8888
		, nodemonPort 			= 2222

		, debugMessages 		= !(require('gulp-load-plugins')().util.env.silent)

module.exports = {
	nodemon: {
		settings: {
			stdout: debugMessages,
			delay: '2888', // delay bc nodemon can be stupid and create two instances sometimes
			env: { 'NODE_ENV': 'development' },
			ignore: ['node_modules/', 'dist/', 'production/'],
			nodeArgs: [`--debug=${nodemonPort}`]
		},
		port: {
			webPort: webPort
		}
	},
	sass: {
		errLogToConsole: false,
		quiet: true,
		sourceComments: false,
		errorBell: true
	},
	paths: {
		js: {
			src: [
        `${appAssets}js/lib/slick.js`,
        `${appAssets}js/**/*.js`,
        `${appAssets}js/*.js`
			],
			dev: [
				`${app}components/**/*.js`,
				`${app}components/*.js`,
				`${appCore}config/**/*.js`,
				`${appCore}config/*.js`
			],
			i18n: [
        `${app}i18n/**/*.js`
      ],
			dest: `${distAssets}js`
		},
		html: {
			src: [
				'app/views/**/*.html'
			],
			dest: appAssets
		},
		ejs: {
			src: [
				'app/core/containers/**/*.ejs',
				'app/i18n/**/*.ejs'
			],
      containers: {
        page: `app/core/containers/page/page.ejs`
      },
			dest: appAssets
		},
    json: {
      i18n: [
        `${app}i18n/**/*.json`
      ]
    },
		css: {
			src: [
				`${appAssets}css/**/*.css`,
				`${appAssets}css/*.css`
			],
      dev: [
        `${appCore}containers/assets-dev/*.css`
      ],
			dest: `${distAssets}css`
		},
		fonts: {
			dev: [
				`${appCore}containers/assets-dev/fonts/responsive/*`
			],
			dest: `${distAssets}css/fonts`
		},
		sass: {
			src: [
				`${appAssets}css/**/*.scss`,
				`${appAssets}css/*.scss`,
        `!${appAssets}css/global/**`
			],
			i18n: [
				`${app}i18n/**/*.scss`
			],
			dest: `${distAssets}css`
		},
		imgs: {
			src: [
				`${appAssets}img/**/*`,
				`${appAssets}img/*`
			],
			// when provided the locale, this fn returns an array of {src: page-img-src, dest: page-img-dest}
			i18n: locale => getDirectories(`app/i18n/${locale}/pages`).map(page => ({
				src: [
					`${app}i18n/${locale}/pages/${page}/img/**/*`,
					`${app}i18n/${locale}/pages/${page}/img/*`
				],
				dest: `${distAssets}img/${page}`
			})),
			dev: [
				`${appCore}containers/assets-dev/img/**/*`,
				`${appCore}containers/assets-dev/img/*`
			],
			dest: `${distAssets}img`
		},
		productionPath: {
			js: `${production}js`,
			css: `${production}css`,
			html: `${production}html`
		}
	}
};
