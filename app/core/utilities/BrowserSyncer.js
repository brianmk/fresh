const gulp = require('gulp')
    , paths = require('../config/settings').paths
    , decache = require('decache')
    , emoji = require('node-emoji')
    , moonSpinner = require('cli-moon-spinner')
    , browserSync = require('browser-sync').create('Fresh BrowserSync Server')
    , nodemon = require('nodemon')
    , syncer = syncType => watcher => {

        let toBeChanged = watcher.hasOwnProperty('tasks')
          ? gulp.watch(watcher.paths, watcher.tasks)
          : gulp.watch(watcher.paths)

        toBeChanged.setMaxListeners(1)

        return toBeChanged.once('change', file => {

          let type = file.type
            , path = file.path.match(/fresh\/app\/(.*)/)[1]

          let changeEmoji = type === 'deleted' ?
            emoji.get('wastebasket')
          : type === 'added' ?
            emoji.get('heavy_plus_sign')
          : emoji.get('pencil2')
          
          console.log(`${changeEmoji}  ${type.charAt(0).toUpperCase() + type.slice(1)} ${path}`)
          
          decache(`app/${path}`)
          
          toBeChanged.removeAllListeners()

          switch(syncType){
            case 'inject': // inject into browser
              console.log(` └──${emoji.get('syringe')}  Injecting updates`)
              break
            case 'refresh': // refresh browser
              console.log(` └──${emoji.get('arrows_counterclockwise')}  Refreshing browser`)
              browserSync.reload()
              break
            case 'restart': // restart app (wait for nodemon)
              moonSpinner({
                textBefore: ' └──',
                textAfter: 'Restarting app',
                stopText: ` └──${emoji.get('seedling')}  Restarting app`
              })(spinner => setTimeout(() => { // delay bc nodemon can be stupid and create two instances sometimes
                browserSync.reload({stream: false})
                spinner.stop(true)
              }, 2888))
              break
          }

          // rewatch
          syncer(syncType)(watcher)
          
        })
      }
    , injector = syncer('inject')
    , refresher = syncer('refresh')
    , restarter = syncer('restart')


browserSync.init({logLevel: 'silent'})

/**
 * these are the variables that store the arrays formed from the 'watchers.map' in 'syncer'.
 * the arrays are comprised of the various gulp.watch's, which return the Node object EventEmitter.
 * removeAllListeners() is used to reset the watchers everytime the browserSync is reset as well (to avoid repeat watchers)
 */
// let injectors, refreshers, restarters
module.exports = () => {

  [
    {paths: paths.sass.src, tasks: ['sass']},
    {paths: paths.fonts.dev, tasks: ['fonts']}
  ].map(watcher => injector(watcher));

  [
    {paths: paths.js.src, tasks: ['js']},
    {paths: paths.ejs.src, tasks: ['ejs']},
    {paths: paths.imgs.src, tasks: ['imgs']},
    {paths: paths.html.src, tasks: ['html']},
    {paths: paths.sass.i18n}
  ].map(watcher => refresher(watcher));

  [
    {paths: paths.js.dev},
    {paths: paths.js.i18n},
    {paths: paths.json.i18n}
  ].map(watcher => restarter(watcher));

}