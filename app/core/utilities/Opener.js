/**
 * open's a page's directory in the user's default program to view files with
 */

const fs = require('fs')
  , open = require('open')

module.exports = (req, res) => {
  
  const openIfExists = (openedType, path) =>
    !fs.existsSync(path)
      ? res.json({err:true, message:`could not open ${type}`})
      : open(path)

  switch(true){
    case req.query.hasOwnProperty('page'):
      
      return locale => {
        const page = req.query.page
          , pageDir = `app/i18n/${locale.lang}_${locale.region}/pages/${page}`

        openIfExists('directory', pageDir)
      }
      break

    case req.query.hasOwnProperty('component'):

      const component = req.query.component
        , componentFilePath = `app/components/${component}.js`

      openIfExists('component file', componentFilePath)
      break
  }

}
