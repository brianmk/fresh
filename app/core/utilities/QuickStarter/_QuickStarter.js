/**
 * creates a page with all the default files
 */

const fs = require('fs')

module.exports = (locale, components) => (req, res) => {
  const page = req.query.page
    , pageDir = `app/i18n/${locale.lang}_${locale.region}/pages/${page}`

  fs.existsSync(pageDir)
    ? res.json({err:true, message:'page taken..'})
    : (() => {

        // make the page's directory
        fs.mkdirSync(pageDir)

        // template
        let templatePath = `${pageDir}/template.ejs`
        fs.writeFileSync(templatePath, require('./template'))

        // data
        fs.writeFileSync(`${pageDir}/data.json`, require('./data'))

        // custom styles
        fs.writeFileSync(`${pageDir}/style.scss`, require('./style'))

        // response
        res.json({
          err: false,
          message: 'successfully added page',
          page
        })

      })()

}
