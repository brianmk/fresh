module.exports = `<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
<p>
  welcome to your new <strong><%- page %></strong> landing page
</p>
<hr>
<p>
  <a href="#" onclick="$.ajax('open?page=<%- page %>')" class="fresh">
    <strong>click here to start editing this page's files</strong>.
  </a>
  <br>
  edits will automatically show up in the browser as long as fresh is running
</p>
<hr>
<p>
  the following message is stored in the <strong>data.json</strong> file:
  <br>
  <i><%- data.message %></i>
</p>
<hr>
<p>
  this &lt;img&gt; is rendered with <a href="#" onclick="$.ajax('open?component=Picture')" class="fresh"><strong>the Picture component</strong></a>:
</p>
<p>
  <%-
    Picture(data.freshLogo, {
      classNames: 'fresh-logo'
    }).html
  %>
</p>
<p>
  note: this &lt;img&gt;'s className was defined in template.ejs
  <br>
  while the src path was defined in data.json.
  <br>
  we're giving you the the option to use whichever method you prefer :)
</p>
<hr>
<p>
  here are two CallToAction component instances:
</p>
<div class="row ctas">
  <%# here, we're storing a new CallToAction instance in a variable "cta" %>
  <% const cta = CallToAction({href: '#', text: 'SHOP THE HANDBAGS'}) %>
  <div class="columns small-6 medium-6 large-6">
    <%# render "cta" as is%>
    <%- cta.html %>
  </div>
  <div class="columns small-6 medium-6 large-6">
    <%# render "cta" with a modification: set the className to "fresh"%>
    <%- cta.htmlWith({classNames: 'fresh'}) %>
  </div>
</div>
<p>
  the styling of the first one is untouched, thus rendered how it would be by default.
  <br>
  the second one is styled with <strong>customized page-specific styles</strong> -
  <br>
  the files for these styles are isolated from the code of the general styles in order to
  <br>
  reduce the amount of overriding and keep the general styles from getting cluttered
</p>`
