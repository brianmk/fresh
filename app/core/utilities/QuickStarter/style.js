module.exports = `body {
    background: snow;
}
#fresh {
    background: snow;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 100vh;
    width: 100vw;
    padding-bottom: 0;
}
.fresh {
    transition: all .8s;
    color: #abc545 !important;
    &:hover, &:focus {
        color: #d0629b !important;
        outline: none !important;
    }
    &-logo {
        * {
            width: 222px;
        }
    }
    &.example {

    }
}
p {
    text-align: center;
    margin: 8px 0;
}
hr {
    width: 288px;
}
.ctas {
    width: 555px;
    max-width: 88vw !important;
    .cta {
        height: 21px;
        &.fresh {
            display: flex;
            align-items: center;
            font-family: 'Lobster', cursive;
            text-transform: lowercase;
            font-size: 18px;
            &:after {
                margin-left: 2px;
                border-style: none;
                content: '\\21DD';
                color: #0a0a0a;
            }
        }
    }
}`
