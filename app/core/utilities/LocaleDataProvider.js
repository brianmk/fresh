const fs = require('fs')
    , gulp = require('gulp')
    , plugins = require('gulp-load-plugins')()
    , genv = plugins.util.env
    , emoji = require('node-emoji')
    , decache = require('decache')

  // combines the strings of language and region into language_REGION
  , clr = (l,r) => `${l.toLowerCase()}_${r.toUpperCase()}`
  // get the language_REGION property from data
  , lrd = (l,r) => {
      let localeDataPath = `../../i18n/${clr(l,r)}/locale-data.json`
        , flag = r.toLowerCase()

      if(r === 'UK' || r === 'uk') flag = 'gb'
      
      decache(localeDataPath) // decache the data in case you are switching between pages (data is modified when pages are served)
      console.log(`${emoji.get(`flag-${flag}`)}  Rendering with locale: ${clr(l,r)}`)
      return require(localeDataPath)
    }

  // determine what the language or region should be
  , determineLR = lrObj => (prop, caseTransform) =>
      lrObj && lrObj.hasOwnProperty(prop)
        ? lrObj[prop][caseTransform]()
        : genv[prop] // can still be falsy
          ? genv[prop][caseTransform]()
          : ''

  , langProp = ['lang', 'toLowerCase'] // language is always lowercase
  , regionProp = ['region', 'toUpperCase'] // uppercase
  , determineBothLR = dlr => [dlr(...langProp), dlr(...regionProp)] // returns an array, which can be decoupled to assign values to lang and region



module.exports = (req = {}) => {
  /**
   * Priority levels for determining the language_REGION to use:
   * 1. Request parameters, ?lang=...&region=... (highest priority)
   * 2. gulp parameters shorthand - gulp --locale ..._... (RECOMMENDED CHOICE)
   * 2. gulp parameters - gulp --lang ... --region ...
   * 3. Check for language_REGION.lock file in dist folder (skip this if only using gulp because the lockfile is just a way of sharing the gulp parameters with other node processes)
   * 4. Fallback: en_US
   */

   let dlr = determineLR(req.query)
     , lang
     , region

  // locale selection shorthand
  if(genv.hasOwnProperty('locale') && genv.locale.length){
    return lrd(...genv.locale.split('_'))
  }

  [lang, region] = determineBothLR(dlr)

  // if both lang and region have been obtained, return them
  if(lang && region) return lrd(lang,region)

  const localePath = 'dist/locale.lock'
  if(fs.existsSync(localePath)){
    let localeFileContent = fs.readFileSync(localePath, 'utf8')
    if(localeFileContent){
      let localeObj = JSON.parse(localeFileContent)
        , dlr = determineLR(localeObj)
        , [lang, region] = determineBothLR(dlr)

      return lrd(lang,region)
    }
  }

  !genv.silent ? console.log(`${emoji.get('interrobang')}  File doesn't exist or is empty.. Resorting to fallback: en_US`) : ''
  return lrd('en','US')

}
