const fs = require('fs')
    , decache = require('decache')
    , LocaleDataProvider = require('./LocaleDataProvider')
    , PagesProvider = require('./PagesProvider')
    , ComponentsProvider = require('./ComponentsProvider')
    , CustomSassCompiler = require('./CustomSassCompiler')
    , QuickStarter = require('./QuickStarter/_QuickStarter')
    , Opener = require('./Opener')
    , BrowserSyncer = require('./BrowserSyncer')
    , WiFiDetector = require('./WiFiDetector')
    , SapientCssCacher = require('./SapientCssCacher')
    , clr = (l,r) => `${l.toLowerCase()}_${r.toUpperCase()}`
    , locale = localeData => clr(localeData.lang, localeData.region)
    , addComponents = components => obj => {
        for(component in components)
          obj[component] = components[component]

        return obj
      }
    , MasterData = fs.readdirSync(`app/i18n`)
        .filter(file => file != '.DS_Store') // just in case OSX adds one of these dumb files
        .reduce((acc, locale) => {
          let localeDataPath = `../../i18n/${locale}/locale-data.json`
          decache(localeDataPath)
          
          acc[locale] = PagesProvider(require(localeDataPath))
          return acc
        }, {})


module.exports = {

  clr,

  locale,

  getRouteArgs: (locale, components) => (production, page) =>  customStyle => {
    if(!production) locale.baseUrl = '/assets/'

    return addComponents(components)({
      master: MasterData,
      locale,
      page,
      data: locale.pages[page],
      customStyle: customStyle,
      production
    })
  },

  
  // packages all the components into an object so that it can be fed to the ejs files
  reduceComponentNamesToComponentsObject: BaseComponent => (accumulator = {}, component) => {

    let componentPath = `../../../${component.path}`
    decache(componentPath)
    let componentClass = require(componentPath)(BaseComponent)
      , componentConstructor = accumulator[component.name] = BaseComponent.componentize(componentClass)

    Object.getOwnPropertyNames(componentClass) // copy all the component's static properties/methods
      .filter(dontCopy => dontCopy!='length' && dontCopy!='name' && dontCopy!='prototype')
      .map(staticProperty => componentConstructor[staticProperty] = componentClass[staticProperty])

    componentConstructor.component = componentClass // component class constructor, just in case it needs to be used somewhere
    return accumulator
  },

  addComponents,

  LocaleDataProvider, // provides the data for a specific locale, given the request (from express), which can be null

  ComponentsProvider,

  Provider: req => { // provides both the page data per language_REGION and the components
    const localeDataWithoutPages = LocaleDataProvider(req)
      , localeData = PagesProvider(localeDataWithoutPages)
      , components = ComponentsProvider(localeData)

    return {localeData, components}
  },

  CustomSassCompiler,

  QuickStarter,

  Opener,

  BrowserSyncer,

  WiFiDetector,

  SapientCssCacher

}
