const fs = require('fs')
    , https = require('https')
    , emoji = require('node-emoji')
    , moonSpinner = require('cli-moon-spinner')
    , cachedSapientCSSPath = `app/core/containers/assets-dev/main.css`
    , cachedSapientCSSPathNew = `app/core/containers/assets-dev/main-new.css`

module.exports = cb => {

  moonSpinner({
    textBefore: ' └──',
    textAfter: 'Caching Sapient\'s CSS',
    stopText: ` └──${emoji.get('floppy_disk')}  Caching Sapient's CSS... Done`
  })(spinner => {

    const getCss = new Promise((res,rej) => {
            https.get('https://int-1.michaelkors.com/assets/styles/main.css', response => {
              let cacheDoesntExist = !fs.existsSync(cachedSapientCSSPath)
                , cacheExistsButIsntUpdated = fs.existsSync(cachedSapientCSSPath) && response != fs.readFileSync(cachedSapientCSSPath)
                , sapientEffedUp = data => data.includes && data.includes('An error occurred while processing your request')
                , newCacheWS = fs.createWriteStream(cachedSapientCSSPathNew)

              response.pipe(newCacheWS).on('close', () => {
                fs.readFile(newCacheWS.path, (err, data) => {
                  if(err || sapientEffedUp(data)) {
                    fs.unlinkSync(cachedSapientCSSPathNew) // delete the file holding the faulty response
                    rej()
                  } else { // the new cache is suitable to be used
                    fs.unlinkSync(cachedSapientCSSPath) // delete the original cache
                    fs.renameSync(cachedSapientCSSPathNew, cachedSapientCSSPath) // rename the new cache
                    res()
                  }
                })
              })

            })
          })
        , minWait = new Promise((res,rej) => setTimeout(() => res(), 888))
        , waitToGetCss = new Promise((res,rej) => Promise.all([getCss, minWait]).then(values => res(values[0])))

    // min wait time of .888s
    async function cache(){ return await waitToGetCss }

    // whichever one finishes first will stop the other from completing
    Promise.race([
      cache(), // cache the css
      new Promise((res,rej) => setTimeout(() => res(), 10e3)), // if waited for 10s already, just stop
    ]).then(res => {
      spinner.stop(true)
      cb()
    })

  })
}