module.exports = lrd => {

  Array.prototype.flatten = function() {return flatten(this)}
  const fs = require('fs')
      , path = require('path')
      , flatten = arr => arr.reduce((acc, val) => acc.concat(Array.isArray(val) ? flatten(val) : val), [])
      , walkSync = dir => fs.readdirSync(dir)
          .filter(file => file.charAt(0) != '_') // dont include files that begin with an underscore (BaseComponent)
          .map(file => fs.statSync(path.join(dir, file)).isDirectory()
            ? walkSync(path.join(dir, file))
            : {
                path: path.join(dir, file).replace(/\\/g, '/'),
                name: file.substr(0, file.lastIndexOf('.js'))
              }
          ).flatten()
          .filter(component => component.path.endsWith('.js')) // only js files
      , {reduceComponentNamesToComponentsObject} = require('../utilities/_utilities')
      , BaseComponent = require('../../components/_BaseComponent')(lrd)

  return walkSync('app/components')
    .map(component => {
      component.path = component.path.replace(/\.[^/.]+$/, '')
      return component
    }) // remove the file extension to prepare it for require()
    .reduce(reduceComponentNamesToComponentsObject(BaseComponent), {BaseComponent: BaseComponent})
  
}
