/**
 * this will provide you with the language_REGION data object WITH the pages object,
 * which includes the data for each page
 */
module.exports = locale => {

  const fs = require('fs')
      , decache = require('decache')
      , localeDir = `/i18n/${locale.lang}_${locale.region}/pages`
      , pages = fs.readdirSync(`app${localeDir}`)
      , asJs = file => `${file}js`
      , asJson = file => `${file}json`
      , readPath = path => `app${path}`
      , requirePath = path => `../..${path}`

  locale.pages = {} // add the pages object to the language_REGION data object.

  return pages
    .filter(file => file != '.DS_Store') // just in case OSX adds one of these dumb files
    .reduce((accumulator, pageName) => {

      let dataPathWithoutExt = `${localeDir}/${pageName}/data.`
        , dataAsJs = asJs(dataPathWithoutExt)
        , dataAsJson = asJson(dataPathWithoutExt)
      
      // in case updates have been made to the json files, refresh the cache
      decache(`..${dataAsJs}`)
      decache(`..${dataAsJson}`)

      const data = fs.existsSync(readPath(dataAsJs)) // first check if data is in js file
              ? require(requirePath(dataAsJs)) // add js data
              : fs.existsSync(readPath(dataAsJson)) && fs.readFileSync(readPath(dataAsJson), 'utf8') // then check if it's in json
                ? require(requirePath(dataAsJson)) // add json data
                : {} // otherwise just add an empty object
      
      accumulator.pages[pageName] = data // add the data to the accumulator

      return accumulator
    }, locale)

}
