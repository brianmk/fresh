const wifiControl = require('wifi-control')
  , moonSpinner = require('cli-moon-spinner')
  , emoji = require('node-emoji')

module.exports = cb => {
  moonSpinner({
    textBefore: '',
    textAfter: 'Detecting Michael Kors WiFi',
    stopText: `${emoji.get('mag_right')}  Detecting Michael Kors WiFi... Done`
  })(spinner => {
    wifiControl.configure({ iface: 'wlan0' })
    wifiControl.scanForWiFi((err, response) => {
      spinner.stop(true)
      if(err) console.log(err)

      let mkWiFiDetected = response.networks.some(network => network.ssid === 'MK0rsData')
    
      mkWiFiDetected
        ? console.log(` ├──${emoji.get('white_check_mark')}  Detected\n   ├──${emoji.get('globe_with_meridians')}  Running app at 'localhost.kors.local:8888'`)
        : console.log(` ├──${emoji.get('warning')}  None detected\n   └──${emoji.get('globe_with_meridians')}  Running app at 'localhost:8888'`)

      cb(mkWiFiDetected) // at least one of the detected networks is the mk wifi
    })

  })
}