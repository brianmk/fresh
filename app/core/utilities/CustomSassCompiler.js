const fs = require('fs')
    , nodeSass = require('node-sass')
    , clr = (l,r) => `${l.toLowerCase()}_${r.toUpperCase()}`
    , localeText = localeData => clr(localeData.lang, localeData.region)
    , sassPath = p => l => `app/i18n/${l}/pages/${p}/style.scss`

/**
 * cb is the callback that will have access to the returned css.
 * The callback is probably going to be the gulp ejs renderer (peep ejs.js or routes.js)
 */
module.exports = locale => (page, cb) => {

  let sp = sassPath(page)
    , localeSass = sp(localeText(locale))
    , sourceSass = locale.pages[page].hasOwnProperty('sources') &&
                    locale.pages[page].sources.hasOwnProperty('style') &&
                    locale.pages[page].sources.style
        ? sp(locale.pages[page].sources.style)
        : localeSass
    
  !fs.existsSync(sourceSass) // if there is no file style.scss in the page's folder
    ? cb(null) // run the callback without any css
    : nodeSass.render({ // run the callback with the compiled css
        data: `
          @import 'app/assets/css/variables/.variables';
          @import 'app/assets/css/mixins/.mixins';
          ${fs.readFileSync(sourceSass, 'utf8')}
        `,
        outputStyle: 'compressed'
      }, (err, result) => {
        if(err){
          cb(`"${err.message}"`)
          console.log(err.message)
        } else {
          cb(result.css.toString())
        }
      })

}