const testPromo = {
        promoType: 'TAI',
        href: '#',
        gallery: {
          pictures: [
            {
              desktop: 'img/collectiondlp/desktop/dlp_collection_desktop-2.jpg',
              mobile: 'img/collectiondlp/mobile/dlp_collection_mobile-2.jpg'
            }
          ]
        },
        legal: 'POWERED BY ANDROID WEAR™ COMPATIBLE WITH IOS AND ANDROID™ <a href="#" title="SEE DETAILS">SEE DETAILS</a>',
        caption: {
          headline: 'LOREM IPSUM',
          text: 'Cea aut unt hicatis undelenda nus, qui co nulpar vollab intorroressi occup.',
          ctas: [
            {
              href: '#',
              text: 'LOREM IPSUM'
            }, {
              href: '#',
              text: 'LOREM IPSUM'
            }
          ]
        }
      }
    , rftp = flagship => require(`./${flagship}`)(testPromo) // require flagship with test promo



module.exports = [
  'TwoUpEvenOption',
  'ThreeUp',
  'ThreeUpOption',
  'FourUp',
  'FourUpOption'
].reduce((acc, flagship) => {
  acc[flagship] = rftp(flagship)
  return acc
}, {})