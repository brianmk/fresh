module.exports = testPromo => ({
    gallery: {
        pictures: [
            {mobile: 'img/componentsdemo/HP_PROMO_1-BIG.gif'},
            {desktop: 'img/collectiondlp/desktop/dlp_collection_desktop-2.jpg'},
            {desktop: 'img/collectiondlp/desktop/dlp_collection_desktop-2.jpg'},
            {desktop: 'img/collectiondlp/desktop/dlp_collection_desktop-2.jpg'}
        ]
    },
    caption: testPromo.caption
})