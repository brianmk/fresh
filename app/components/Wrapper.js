/**
 * constructor:
 * 
 */

module.exports = BaseComponent => class Wrapper extends BaseComponent {

  get html(){
    return `
      <section class="row mkwp--wrapper ${this.classNames || ''}">
        ${this.data.content}
      </section>
    `
  }

}