/**
 * constructor:
 * features [...Promo] - array of 3 promos
 * extra <any html> (optional) - any bonus html
 */

module.exports = BaseComponent => {

  const {Anchor, Promo} = BaseComponent.import('Anchor', 'Promo')

  return class DestinationKors extends BaseComponent {

    get html(){

      return `
        <article class="destination-kors ${this.classNames}">
          ${
            !this.data.extra ? '' : `
              <div class="destination-kors--extra">
                ${this.data.extra}
              </div>
            `
          }
          <div class="destination-kors--features">
            ${this.data.features.map(feature => Promo(feature).html).join('')}
          </div>
        </article>
      `
    }


  }
}