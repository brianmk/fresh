module.exports = BaseComponent => {

  const {Picture} = BaseComponent.import('Picture')

  return class Look extends BaseComponent {
    
    get html(){
      return `
        <article class="expandable-collection ${this.classNames}">
          <input class="expandable-collection--toggle--checkbox" id="${this.id}" type="checkbox" />
          <div class="expandable-collection--collection">
            ${
              this.data.looks.map(look => `
                ${
                  Picture(look, {
                    classNames: 'columns medium-3',
                    href: '#'
                  }).html
                }
              `).join('')
            }
          </div>
          <label class="expandable-collection--toggle--button" for="${this.id}">
            <span class="expandable-collection--expand-text">${this.data.expandText}</span>
            <span class="expandable-collection--collapse-text">${this.data.collapseText}</span>
          </label>
        </article>
      `
    }
  }
}