module.exports = BaseComponent => {

  const {Anchor} = BaseComponent.import('Anchor')

  /**
   * constructor:
   * href <string>
   * text <string>
   */
  return class CallToAction extends BaseComponent {
    get html(){
      let formal = BaseComponent.formalize(this.data.text)
      return Anchor({
        classNames: ['cta'],
        bem: this.data.bem || {},
        title: formal,
        alt: formal,
        href: this.href,
        content: this.data.text
      }).html
    }
  }
}
