module.exports = BaseComponent => {

  const {Picture} = BaseComponent.import('Picture')

  return class Collection {

    pictureElement(collectionImage, className=''){
      return `
        <a href="${collectionImage.href}" class="collection--picture ${className}">
          ${Picture(collectionImage).html}
        </a>
      `
    }

    wideCollectionImage(collectionImage){
      let imagePart = collectionImage.width
        , pictureElements = ''

      while(imagePart--)
        pictureElements += this.pictureElement(collectionImage, `image${collectionImage.width-imagePart} of${collectionImage.width}`)

      return pictureElements
    }

    collectionImage(collectionInstance){
      return (collectionImage, collectionImageIndex) =>
        !collectionImage.hasOwnProperty('width')
          ? collectionInstance.pictureElement(collectionImage)
          : collectionInstance.wideCollectionImage(collectionImage)
    }

    get html(){
      return `
        <section class="row collection small-uncollapse medium-collapse large-collapse ${this.classNames}">
          <div class="collection--slider small-12 medium-12 large-12 columns">
            ${this.data.map(
              this.collectionImage(this)
            )}
          </div>
        </section>
      `
    }
  }
}
