/**
 * constructor:
 * text <string>: the quote text
 * quoter <bool> (optional): whether or not to include quoter's name underneath
 */

module.exports = BaseComponent => class Quote extends BaseComponent {
  get html(){
    return `
      <article class="quote ${this.classNames}" ${this.data.quoter ? `quoter="${this.data.quoter}"` : ''}>
        <q class="small-12 medium-8 large-8 columns">${this.data.text}</q>
      </article>
    `
  }
}
