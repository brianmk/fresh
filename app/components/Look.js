module.exports = BaseComponent => {

  const {Picture, CallToAction} = BaseComponent.import('Picture', 'CallToAction')

  return class Look extends BaseComponent {

    get sectionClassName(){
      return 'row look small-collapse medium-collapse large-collapse'
    }

    // for when an image has no desktop version
    noDesktopClassName(image){
      return !image.hasOwnProperty('desktop')
        ? 'no-desktop '
        : ' '
    }

    // size and position of look--image (on desktop)
    imageSPClassName(imageIndex){
      let look = this.data
      return this.isSingle
        ? ` medium-${look.colspan} large-${look.colspan} medium-centered large-centered `
        : imageIndex > 0
          ? look.ltr
            ? ' medium-4 large-4 medium-offset-1 large-offset-1 vCenter-right '
            : ' medium-6 large-6 medium-offset-1 large-offset-1 '
          : look.ltr
            ? ' medium-6 large-6 '
            : ' medium-4 large-4 medium-offset-1 large-offset-1 vCenter-left '
    }

    lookImageClassName(image, imageIndex){
      return ' look--image small-12 columns '
        + this.noDesktopClassName(image)
        + this.imageSPClassName(imageIndex)
    }

    get isSingle(){
      return this.data.images.filter(
        image => image.hasOwnProperty('desktop')
      ).length === 1
    }

    get hrefLocationClassName(){
      return this.isSingle ? this.data.hrefLocation : ''
    }

    lookImageNeedsCta(imageIndex){
      return this.isSingle
        ? true
        : imageIndex > 0
          ? this.data.ltr
          : !this.data.ltr
    }

    get lookImageCtaClassName(){
      return ` look--cta ${this.hrefLocationClassName} `
    }

    lookImageCta(imageIndex){
      let lookInstance = this
      return this.lookImageNeedsCta(imageIndex)
        ? CallToAction(lookInstance.cta, {
            className: lookInstance.lookImageCtaClassName
          }).html
        : ''
    }

    lookImage(lookInstance){
      return (image, imageIndex) => `
        <div class="${lookInstance.lookImageClassName(image, imageIndex)}">
          <a href="${lookInstance.href}">
            ${Picture(image).html}
          </a>
          ${this.lookImageCta(imageIndex)}
        </div>
      `
    }

    get html(){
      this.cta = {
        href: this.href,
        text: this.lrd('pages.trends.looksCtaText')
      }

      return `
        <section ${this.id} class="${this.sectionClassName} ${this.classNames}">
          ${this.data.images.map(
            this.lookImage(this)
          )}
        </section>
        ${
          CallToAction(this.cta, {
            classNames: 'look--cta'
          }).html
        }
      `
    }
  }

}
