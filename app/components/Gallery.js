/**
 * constructor:
 * pictures <array> (optional): array of <Picture>s
 */

// @TODO: disallow nested anchors

module.exports = BaseComponent => {
  const {Picture, Carousel, Anchor} = BaseComponent.import('Picture', 'Carousel', 'Anchor')
  return class Gallery extends BaseComponent {

    pictures(){
      return this.data.pictures
        .map((pic, index, pics) => Picture(pic, {alt: this.alt}).html)
        .join('')
    }
    
    get html(){

      return this.data.hasOwnProperty('href') && this.data.href
        ? Anchor.wrap(this).html
        : `
            <div class="${this.classNames} ${this.data.carousel ? 'carousel' : ''}">
            ${
              this.data.hasOwnProperty('pictures') && Object.keys(this.data.pictures).length > 0
                ? this.pictures()
                : ''
            }
            </div>
            ${this.data.carousel ? Carousel.slicker(this.data.carousel) : ''}
          `
    }


  }
}
