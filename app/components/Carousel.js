/**
 * constructor:
 * items Array<content> - array of content. content can be any html
 * options <object> - slick options (http://kenwheeler.github.io/slick/)
 */

const util = require('util')
    , carouselArrow = prev => `
        <svg class="slick-arrow carousel--arrow ${prev ? 'carousel--arrow__prev' : 'carousel--arrow__next'}" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 17.5 52" style="enable-background:new 0 0 17.5 52;" xml:space="preserve">
          <style type="text/css">
            .st0{fill-rule:evenodd;clip-rule:evenodd;fill:#D7D7D7;}
          </style>
          <path class="carousel--arrow-path st0" d="M0,52l15.7-26L0,0h1.8l15.7,26L1.8,52H0z"/>
        </svg>
      `.replace(/"/g, '&quot;') // escape the double quotes because they appear in an html attribute
    
    // this fn, provided the slick options, outputs the html that will slick the PREVIOUS element
    , slicker = options => `
        <style scoped="scoped" onload=" ${/* slick on load */''}
          const waitForJQuery = cb =>
            typeof $ !== 'undefined' && typeof $().slick == 'function'
              ? cb()
              : setTimeout(() => waitForJQuery(cb), 250)
          ;
          waitForJQuery(() => {
            $(this).prev().on('init', s => {
              setTimeout(() => {
                $(window).trigger('resize'); ${/* because images are not immediately displayed when going to another page and then coming back to this one (Sapient) */''}
                $(this).remove(); ${/* remove this element after slicking has completed */''}
              }, 888); ${/* wait a little bit to let the page content load before resizing the window  */''}
            }).slick(${ // slick options
              options ? util.inspect(options, false, null) : ''
            });
          });
        "></style>
      `.replace(/(\r\n|\n|\r)/gm,'') // remove line breaks because in html attr
    

module.exports = BaseComponent => class Carousel extends BaseComponent {

  static slicker(options){
    return slicker(options)
  }

  get html(){
    if(this.data.options){
      this.data.options.prevArrow = carouselArrow(true)
      this.data.options.nextArrow = carouselArrow(false)
    } else { // default options
      this.data.options = {
        adaptiveHeight: true, infinite: true, speed: 500, slidesToShow: 1, slidesToScroll: 1, 
        arrows: false, dots: true, cssEase: 'ease', autoplaySpeed: 1000, autoplay: true,
        prevArrow: carouselArrow(true), nextArrow: carouselArrow(false), mobileFirst: true,
        pauseOnHover: true, pauseOnFocus: true,
        responsive: [{ breakpoint: 767, settings: 'unslick' }]
      }
    }

    return this.data.hasOwnProperty('href') && this.data.href
      ? BaseComponent.import('Anchor').Anchor.wrap(this).html
      : `
        <article class="carousel ${this.classNames}">
          ${this.data.items.join('')}
        </article>
        ${slicker(this.data.options)}
      `

    
  }
}