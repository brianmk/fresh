const {fromJS, isImmutable, Map, List, Set} = require('immutable')
    , {reduceComponentNamesToComponentsObject} = require('../core/utilities/_utilities')
    , isString = (...test) => test.every(t => typeof t === 'string' || t instanceof String)
    , convertStringToArray = s => s.trim().split(/\s+/) // split by words (with any amount of spaces between)
    , compileBEM = (bem = {}) => {
        let hasBlock = bem.hasOwnProperty('block')
          , hasElement = bem.hasOwnProperty('element')
          , hasModifiers = bem.hasOwnProperty('modifiers')
          , bemClasses = ' '

        if(!hasBlock) {
          // throw 'BEM classes cannot exist without a block'
          // console.error('BEM classes cannot exist without a block')
          return ''
        }
        
        if(!hasElement || !bem.element) bemClasses += bem.block

        if(hasElement) bemClasses += ` ${bem.block}--${bem.element}`

        if(hasModifiers) bemClasses += bem.modifiers.map(m => ` ${bem.block}${hasElement ? `--${bem.element}` : ''}__${m}`).join('')

        return bemClasses.trim()
      }
    , formalizeText = text => text
        .replace(/(<br \/>|<br\/>|<br>)/g, ' ') // replace html line breaks with a space
        .replace(/[""'']/g, '') // strip out any quotation marks
        .toLowerCase() // make all text lowercase
        .replace(/\b\w/g, l => l.toUpperCase()) // capitalize the first letter of each word
    , arrayUnion = arr1 => arr2 => Array.from(new Set(arr1.concat(arr2))) // returns the union (no duplicates) of two arrays
    , cloneData = data => JSON.parse(JSON.stringify(data)) // cloning is important because you want to keep original data immuted
    , mergeTwoData = (oldData, newData) => {
        if(!isImmutable(oldData)) oldData = Map(oldData)
        if(!isImmutable(newData)) newData = Map(newData)
        
        return oldData
          .mergeDeepWith((oldDatum, newDatum, key) => {
            if(key === 'classNames'){
              oldDatum = isString(oldDatum) ? Set(convertStringToArray(oldDatum)) : Set(oldDatum)
              newDatum = isString(newDatum) ? Set(convertStringToArray(newDatum)) : Set(newDatum)
              return Set.union([oldDatum, newDatum])
            } else if(Array.isArray(oldDatum) || Array.isArray(newDatum)) {
              if(!Array.isArray(oldDatum)) oldDatum = [oldDatum]
              if(!Array.isArray(newDatum)) newDatum = [newDatum]
              return oldDatum.concat(newDatum)          
            } else if(typeof oldData === 'object' && typeof newDatum === 'object'){
              return mergeData(Map(oldDatum), Map(newDatum))
            } else {
              return newDatum
            }
          }, newData)
          .toJS()
      }
    , mergeData = (oldData, ...newData) => newData.reduce(mergeTwoData, oldData)



/**
 * The BaseComponent is the class that all components will extend from,
 * thereby inheriting all of the BaseComponent's helper methods.
 * The BaseComponent is initialized with the local-data.json of the focal locale
 *
 * constructor:
 * id <string> (optional): html id of dom element
 * classNames <string> (optional): html class names of dom element
 * href <string> (optional): href attribute of dom element
 */
module.exports = lrd => class BaseComponent {

  lrd(property = undefined){
    return property
      ? eval(`lrd.${property}`) // return data for specified property (nested properties allowed, ex: pages.trends.looksCtaText)
      : lrd // otherwise it will return all of the language_REGION data
  }

  get baseUrl(){ // shorter method for obtaining baseUrl using getter
    return lrd.baseUrl
  }

  get id(){ // prints out the id attribute of a component
    return this.data.id // if id is provided
      ? this.data.id // use the provided id
      : Number.isInteger(this.data.index) // if an index is provided (and it's an integer)
        ? this.constructor.name.toLowerCase()+this.data.index // use the index
        : '' // otherwise don't give the component an id
  }

  get classNames(){ // prints out the provided className for a component
    if(isString(this.data.classNames)) return this.data.classNames.trim()
    if(Array.isArray(this.data.classNames)) return this.data.classNames.join(' ').trim()
  }

  addClassNames(...newClassNames){ // adds the provided className to the list of classNames and then returns the printed list (if print === true)
    let constructorName = this.constructor.name
      , componentCloneData = JSON.parse(JSON.stringify(this.data))

    componentCloneData.classNames = arrayUnion(this.data.classNames)(
      newClassNames
        .reduce((acc, ncn, i) => acc.concat(ncn), []) // flatmap
        .map(ncn => ncn.trim())
    )
    return BaseComponent.import(constructorName)[constructorName](componentCloneData)
  }
  

  get href(){
    let href = this.data.href || ''
      , ndl = '[NON_DEFAULT_LOCALE]'

    return /^(?:[a-z]+:)?\/\//i.test(href) ? href // absolute url
      : lrd.hasOwnProperty('nonDefaultLocale') && lrd.nonDefaultLocale && !href.startsWith(ndl)
        ? ndl + href
        : href
  }

  get attr(){
    let attrs = ''
    if(this.data.attr) for (let attr in this.data.attr)
      attrs += `${attr}="${this.data.attr[attr]}" `
    
    return attrs
  }

  get bem(){ // returns all the class names of BEM (http://getbem.com/naming/)
    if(!this.data.bem) return '' // if no bem provided, just return empty string

    return Array.isArray(this.data.bem)
      ? this.data.bem.map(b => compileBEM(b)).join(' ').trim()
      : compileBEM(this.data.bem)
  }

  htmlWith(assignor = {}){ // in case you want to change any data before you render
    // let ret = this.cloneWith(assignor).html
    // console.inspect(ret)
    // return ret
    
    let clone = JSON.parse(JSON.stringify(this))
      , constructorName = this.constructor.name
    
    return BaseComponent.import(constructorName)[constructorName](Object.assign(clone.data, assignor)).html
  }


  static formalize(text=''){ // formalizes text (usually for the title or alt attribute of an element)
    return text.replace // if text arg has the fn 'replace' (probably means text arg is string)
      ? formalizeText(text)
      : text.text ? formalizeText(text.text) : '' // for when text arg is an object that has the property 'text', where the text value is stored
  }

  static bem(bem = {}){
    return compileBEM(bem)
  }

  static addBemTo(data, addBem = {}){ // the block and element in newBem will override but the modifiers will will be unified (no duplicates)
    let existingBem = data.bem || {}
      , bemClone = cloneData(existingBem)

    if(!existingBem.hasOwnProperty('block') && !addBem.hasOwnProperty('block')){
      console.error('A block must exist in at least either of existingBem or addBem')
      return
    } else {
      Object.assign(bemClone, addBem) // override the BEMs if provided
      if(Array.isArray(existingBem.modifiers)) // merge existing modifiers if they were present
        bemClone.modifiers = arrayUnion(existingBem.modifiers)(addBem.modifiers)

      // make a copy of the original data but update the copy's bem to the extended one
      let dataClone = cloneData(data)
      dataClone.bem = bemClone
      return dataClone // return the updated data copy
    }
  }

  static addClassNamesTo(data = {}, ...newClassNames){ // adds newClassNames to the data's classNames array and returns the new data
    let clone = cloneData(data)
    if(!clone.hasOwnProperty('classNames')) clone.classNames = []

    clone.classNames = arrayUnion(clone.classNames)(newClassNames.map(ncn => ncn.trim()))
    return clone
  }

  static combineClassNames(classNames = [], newClassNames = [], print = false){
    let union = arrayUnion(classNames)(newClassNames.map(ncn => ncn.trim()))
    return print ? union.join(' ') : union
  }

  static cloneData(data){
    return cloneData(data)
  }

  static clone(instance){ // for the sake of immutability, this will return a clone (new instance) of the component instance provided
    let constructorName = instance.constructor.name
    return BaseComponent.import(constructorName)[constructorName](instance.data)
  }

  get clone(){
    return BaseComponent.clone(this)
  }

  cloneWith(data = {}){
    let cloneInstance = this.clone
    return Object.assign(cloneInstance, {data: data})
  }

  /**
   * data is the object containing the data that will be used to render the component.
   * data can be passed from data.json or created when the contructor is invoked.
   * assignor is an object whose properties will extend data's if they don't exist and replace data's if they do exist
   */
  static componentize(newComponentClass){
    return (data = {}, assignor = {}) => {
      
      // allow components to be constructed with just text. doing so will create an object with the string as the object's text property
      if(isString(data)) data = {text:data}

      // default data
      data.id = data.id || ''
      data.alt = data.alt || ''
      data.title = data.title || ''
      data.attr = data.attr || {}
      data.classNames = isString(data.classNames) // classNames are allowed to be provided as strings, but they will be converted to arrays
        ? data.classNames.trim().replace(/\s\s+/g, ' ').split(' ') // reduce extended spaces to just one space and then convert to array
        : Array.isArray(data.classNames)
          ? data.classNames.map(cn => cn.trim())
          : []
      
      let newComponent = new newComponentClass // constructor doesnt work for whatever reason ._., so we'll have to construct the instance like this
      newComponent.data = Object.assign(cloneData(data), assignor) // first clone the data to keep the original data object immutable
      
      return newComponent
    }
  }

  static union(...items){
    return Set.union(items.map(item => !Array.isArray(item) ? Set([item]) : Set(item))).toJS()
  }

  set(key, value){
    return this.cloneWith(fromJS(this.data).set(key, value).toJS())
  }

  merge(...newData){ // concatentate strings and arrays
    return this.cloneWith(mergeData(this.data, ...newData))
  }

  updateIn(keyPath = [], updater){
    return this.cloneWith(
      fromJS(this.data)
        .updateIn(keyPath, updater)
        .toJS())
  }

  // update(){ // override

  // }

  static mergeData(oldData, ...newData){
    return mergeData(oldData, ...newData)
  }

  /**
   * imports component dependencies inside other components.
   * to import components within nested directories, include the directory in the import string.
   * ex: BaseComponent.import('flagships/FourUpOption')
   */
  static import(...componentPaths){
    return componentPaths
      .map(componentPath => {
        let compMatch = componentPath.match(/[a-zA-Z]+/g) // split the string fed throguh BaseComponent.import and use the last full word as the component name
          , compName = compMatch[compMatch.length - 1] // @TODO: add support for component files that have the same name but are in different directories. figure out how to assign them unique names because they are all attached as properties to the same object in the components reducer
        
        return {
          path: `app/components/${componentPath}`,
          name: compName
        }
      })
      .reduce(reduceComponentNamesToComponentsObject(this), {})
  }

}
