/**
 * constructor:
 * text <string>
 */
module.exports = BaseComponent => class Text extends BaseComponent {
  get html(){
    return `
      <p class="${this.bem || ''} ${this.classNames || ''}">
        ${this.data.text}
      </p>
    `
  }
}
