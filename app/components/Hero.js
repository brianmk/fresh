module.exports = BaseComponent => {

  const {Picture, CallToAction, Anchor} = BaseComponent.import('Picture', 'CallToAction', 'Anchor')

  return class Hero extends BaseComponent {

    get collapseClassName(){
      return this.data.full
        ? 'medium-collapse large-collapse'
        : 'medium-uncollapse large-uncollapse'
    }

    // class names for size and position (offset) on desktop
    get desktopSP(){
      return this.data.textRight
        ? 'medium-offset-8 large-offset-8'
        : 'medium-offset-1 large-offset-1'
    }

    get heroImage(){
      const hero = this.data
        , heroImage = Picture(hero.image).html

      return hero.ctas
        ? Anchor({
            href: hero.ctas[hero.ctas.length - 1].href,
            content: heroImage
          }, {
            classNames: 'small-12 medium-12 large-12 small-centered medium-centered large-centered columns hero--image'
          }).html
        : heroImage
    }

    get html(){
      const hero = this.data

      return `
        <section ${this.id} class="row hero small-collapse ${this.collapseClassName} ${this.classNames}">
          ${this.heroImage}
          <div class="hero--text small-12 medium-3 large-3 ${this.desktopSP} columns">
            ${
              hero.hasOwnProperty('surtitle')
                ? `<h3 class="hero--text--surtitle">${hero.surtitle}</h3>` : ''
            }
            <h2 class="hero--text--header">
              ${hero.header}
            </h2>
            ${
              hero.hasOwnProperty('subheader')
                ? `<h3 class="hero--text--subheader">${hero.subheader}</h3>` : ''
            }
            ${
              hero.hasOwnProperty('ctas')
                ? hero.ctas.map(cta => CallToAction(cta, {className: 'hero--text--cta'}).html)
                : ''
            }
          </div>
        </section>
      `
    }
  }
}
