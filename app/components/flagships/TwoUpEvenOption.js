/**
 * constructor:
 * 
 */

module.exports = BaseComponent => {
  const {Flagship, Promo} = BaseComponent.import('Promo', 'flagships/_Flagship')
  return class TwoUpEvenOption extends Flagship.component {

    get html(){

      return this.flagshipWrapper
        .merge({
          content: Promo(this.data)
            .updateIn(['gallery', 'pictures'], pictures =>
              pictures.map(p =>
                BaseComponent.mergeData(p, {classNames: ['columns', 'small-5']})))
            .html
        }).html
      
    }
  }
}