
module.exports = BaseComponent => {
  const {Wrapper} = BaseComponent.import('Wrapper')
  return class Flagship extends BaseComponent {

    get flagshipWrapper(){
      return Wrapper({
        classNames: BaseComponent.union('flagship', `flagship${this.constructor.name}`, this.data.classNames) // @TODO: clean this up
      })
    }

  }
}