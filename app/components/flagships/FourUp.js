/**
 * constructor:
 * 
 */

module.exports = BaseComponent => {
  const {Flagship, Promo} = BaseComponent.import('flagships/_Flagship', 'Promo')

  return class FourUp extends Flagship.component {

    get html(){
      return this.flagshipWrapper
        .merge({
          content: this.data.promos
            .map(promo => Promo(promo).addClassNames(['columns', 'small-6', 'medium-3']).html)
            .join('')
        }).html
      
    }

  }
}