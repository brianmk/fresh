/**
 * constructor:
 * 
 */

module.exports = BaseComponent => {
  const {Flagship, Promo} = BaseComponent.import('flagships/_Flagship', 'Promo')
  return class ThreeUpOption extends Flagship.component {

    get html(){
      let cloneData = BaseComponent.cloneData(this.data)
      cloneData.gallery.pictures = cloneData.gallery.pictures.map(p =>
        BaseComponent.addClassNamesTo(p, 'columns', 'small-12', 'medium-4'))

      return this.flagshipWrapper
        .htmlWith({
          content: Promo(cloneData, {
            promoType: 'TAI'
          }).html
        })
    }

  }
}