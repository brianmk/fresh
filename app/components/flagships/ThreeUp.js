/**
 * constructor:
 * 
 */

module.exports = BaseComponent => {
  const {Flagship, Promo, Carousel} = BaseComponent.import('flagships/_Flagship', 'Promo', 'Carousel')
  return class ThreeUp extends Flagship.component {

    get html(){

      return this.flagshipWrapper
        .htmlWith({
          content: Carousel({
            items: this.data.promos.map(p => Promo(p).addClassNames(
              'columns', 'medium-4'
            ).html)
          }).html
        })
    }

  }
}