/**
 * constructor:
 * 
 */

module.exports = BaseComponent => {
  const {Flagship, Promo, Wrapper} = BaseComponent.import('flagships/_Flagship','Promo', 'Wrapper')
  return class FourUpOption extends Flagship.component {

    get html(){

      let galleryClone = JSON.parse(JSON.stringify(this.data.gallery))
      galleryClone.pictures = this.data.gallery.pictures.map(picture => {
        picture.classNames = BaseComponent.combineClassNames(picture.classNames, ['columns', 'small-6', 'medium-3'])
        return picture
      })

      return this.flagshipWrapper
        // .merge({classNames: ['small-collapse','medium-uncollapse']})
        .addClassNames('small-collapse','medium-uncollapse')
        .set('content',
          Promo({
            promoType: 'TAI',
            gallery: galleryClone,
            caption: this.data.caption
          }).html
        ).html

      
    }

  }
}
