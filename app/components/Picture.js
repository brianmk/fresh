/**
 * constructors:
 * mobile <string> (optional) - path to mobile image file
 * desktop <string> (optional) - path to desktop image file
 * alt <string> (optional) - alternative text
 * href <string> (optional) - if provided, wraps picture in an anchor tag which will be linked to the provided href
 */
module.exports = BaseComponent => {

  const {Anchor} = BaseComponent.import('Anchor')

  return class Picture extends BaseComponent {

    get html(){

      let filename = ((this.data.mobile) || (this.data.desktop || '')).replace(/^.*[\\\/]/, '')
        , alt = this.data.hasOwnProperty('alt') ? BaseComponent.formalize(this.data.alt) || filename : filename
        , hasMobile = this.data.hasOwnProperty('mobile')
        , hasTablet = this.data.hasOwnProperty('tablet')
        , hasDesktop = this.data.hasOwnProperty('desktop')
        , hiders = ''
        , fallbackImgPath = this.baseUrl + (this.data.desktop || this.data.tablet || this.data.mobile)

      if(hasMobile && !hasDesktop) hiders = 'hide-for-medium'
      if(!hasMobile && hasDesktop) hiders = 'hide-for-small-only'

      return this.data.hasOwnProperty('href') && this.data.href
        ? Anchor.wrap(this).html
        : hasDesktop || hasTablet || hasMobile ? `
            <picture class="${this.bem} ${this.classNames} ${hiders}">
              ${ // if has desktop AND tablet, include source for tablet. otherwise just use desktop for tablet-size and above
                !hasDesktop ? '' : hasTablet
                  ? `
                      <source srcset="${this.baseUrl+this.data.desktop}" media="(min-width:1025px)">
                      <source srcset="${this.baseUrl+this.data.tablet}" media="(min-width:768px)">
                    `
                  : `<source srcset="${this.baseUrl+this.data.desktop}" media="(min-width:768px)">`
              }
              ${
                hasMobile
                  ? `<source srcset="${this.baseUrl+this.data.mobile}" media="(max-width:767px)">`
                  : ''
              }
              <img src="${fallbackImgPath}" tabindex="0" alt="${alt}" title="${alt}" ${this.attr}>
            </picture>
          ` : ''
    }
  }
}