/**
 * constructor:
 * href <string>
 * content <string>
 */
module.exports = BaseComponent => class Anchor extends BaseComponent {

  // use the provided component instance to create an anchor with the component's rendered html as it's content
  static wrap(component){

    // clone the data for the new component that will be wrapped
    let clearedData = JSON.parse(JSON.stringify(component.data))
    
    // empty the component's href to prevent an infinite loop of Anchor.wrap wrapping itself
    delete clearedData.href;
    
    // empty other data that you want on the anchor rather than the content
    delete clearedData.bem;
    clearedData.id = ''
    clearedData.classNames = []
    
    return BaseComponent.import('Anchor').Anchor(component.data, {
      content: Object.assign(component, {data: clearedData}).html
    })

  }

  get html(){
    let formal = BaseComponent.formalize(this.data.alt || this.data.title || this.data.text ||'')

    return `
      <a href="${this.href}" class="${this.classNames} ${this.bem}" alt="${formal}" title="${formal}">
        ${this.data.content}
      </a>
    `
  }

}
