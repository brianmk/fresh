/**
 * constructor:
 * slug <string | Slug> (optional)
 * headline <string | Headline> (optional)
 * text <string | Text> (optional)
 * ctas <array | CTAs> (optional): array of <CallToAction>s
 */

const util = require('util')
    , isString = toCheck => typeof toCheck === 'string' || toCheck instanceof String
    , isArray = toCheck => typeof toCheck instanceof Array || Array.isArray(toCheck)
    , printClasses = (...classes) => classes.join('').trim()

module.exports = BaseComponent => {
  const {CallToAction, Anchor, Text} = BaseComponent.import('CallToAction', 'Anchor', 'Text')
  return class Caption extends BaseComponent {

    get captionAlt(){
      return this.data.headline ?
          this.data.headline
        : this.data.slug ?
          this.data.slug
        : this.data.text ?
          this.data.text
        : this.data.ctas ?
          this.data.ctas[0].text
        : ''
    }


    get html(){

      const captionElementText = el => isString(this.data[el]) || isArray(this.data[el])
              ? this.data[el]
              : this.data[el].text || ''
          , captionElementClassNames = el => this.data[el].classNames ? this.data[el].classNames.join(' ') : ''
          , captionElementBEM = el => this.data[el].bem ? BaseComponent.bem(this.data[el].bem) : ''
      
      return `
        <div class="${this.bem || ''} ${this.classNames || ''}">
          ${
            this.data.hasOwnProperty('slug')
              ? `
                  <div class="${printClasses(captionElementClassNames('slug'), captionElementBEM('slug'))}">
                    ${captionElementText('slug')}
                  </div>
                `
              : ''
          }
          ${
            this.data.hasOwnProperty('headline')
              ? this.href
                ? Anchor({
                    href: this.href,
                    title: this.data.headline,
                    content: `<h2>${captionElementText('headline')}</h2>`,
                    classNames: [captionElementClassNames('headline'), captionElementBEM('headline')]
                  }).html
                : `
                    <div class="${printClasses(captionElementClassNames('headline'), captionElementBEM('headline'))}">
                      <h2>${captionElementText('headline')}</h2>
                    </div>
                  `
              : ''
          }
          ${
            !this.data.text ? '' : (() => {
              this.data = BaseComponent.addClassNamesTo(this.data, captionElementClassNames('text') + captionElementBEM('text'))
              
              let text = Text(this.data.text)

              return this.data.hasOwnProperty('href') && this.data.href
                ? Anchor.wrap(text).html
                : text.html

            })()
          }
          ${
            !this.data.hasOwnProperty('ctas') ? '' : `
              <div class="${printClasses(captionElementClassNames('ctas'), captionElementBEM('ctas'))}">
                ${
                  (isArray(this.data.ctas) ? this.data.ctas : this.data.ctas.ctas)
                    .map(a => CallToAction(a, {content: a.text}).html)
                    .join('')
                }
              </div>
            `
          }
        </div>
      `
    }


  }
}
