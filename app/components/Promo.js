/**
 * constructor:
 * promoType <string>: TAI, TOI, Multi
 * gallery <Gallery> (optional)
 * caption <Caption> (optional)
 */

module.exports = BaseComponent => {
  const {Caption, Gallery, Text} = BaseComponent.import('Caption', 'Gallery', 'Text')
      , isString = test => typeof test === 'string' || test instanceof String

  return class Promo extends BaseComponent {

    get html(){
      if(!this.data.promoType) console.log('Promo requires promoType')

      // make sure the caption component in this promo has the correct BEM data
      let caption, gallery, pictures
      if(this.data.caption){

        let captionBem = {block:'promo', element:'caption'}
        this.data.caption.hasOwnProperty('bem') && this.data.caption.bem.hasOwnProperty('modifiers')
          ? captionBem.modifiers = this.data.caption.bem.modifiers : ''
        
        caption = Caption(this.data.caption, {href: this.href, bem: captionBem})
        
        ;['slug','headline','text','ctas'].map(el => {
          if(caption.data.hasOwnProperty(el)){
            if(caption.data[el].hasOwnProperty('bem')){
              caption.data[el].bem.block = 'promo'
              caption.data[el].bem.element = el
            } else if(typeof caption.data[el] === 'string' || caption.data[el] instanceof String ) {
              // if the data is in string form, convert it to an object with the bem and a property "text" with the string
              caption.data[el] = {
                text: caption.data[el],
                bem: {block:'promo', element:el}
              }
            } else if(Array.isArray(caption.data[el])){
              // if the data is in array form, convert it to an object with the bem and a property, named after the element, with the original array
              var arrayClone = BaseComponent.cloneData(caption.data[el])
              caption.data[el] = {
                [el]: arrayClone,
                bem: {block:'promo', element:el}
              }
            } else {
              // a normal object that didnt have bem data - add bem to it
              caption.data[el].bem = {block:'promo', element:el}
            }
          }
        })

        if(caption.data.hasOwnProperty('ctas')){
          caption.data.ctas.ctas = caption.data.ctas.ctas.map(cta => {
            let ctaClone = BaseComponent.cloneData(cta)
            ctaClone.bem = {block:'promo', element:'cta'}
            return ctaClone
          })
        }

        if(this.data.gallery){ // if gallery provided
          gallery = Gallery(this.data.gallery, {alt: caption.captionAlt}).addClassNames('promo--picture-container')
          pictures = gallery.data.pictures

          // add promo--picture className to each picture
          gallery.data.pictures = pictures.map(picture => BaseComponent.addBemTo(picture, {block:'promo', element:'picture'}))
        }

      }

      let legalComponentData = this.data.legal
      if(isString(this.data.legal)){ // if string, convert to object so you can include bem data
        legalComponentData = {
          text: this.data.legal,
          bem: { block:'promo', element:'legal' }
        }
      }


      let bem = BaseComponent.bem(BaseComponent.addBemTo(this.data, {block: 'promo'}).bem) // @TODO: make this into a method (ex. this.bem({bemdata}))

      return `
        <article id="${this.id}" class="promo${this.data.promoType} ${bem} ${this.classNames}" ${this.attr}>
          ${this.data.gallery ? gallery.html : ''}
          ${this.data.legal ? Text(legalComponentData).addClassNames('small-order-3','medium-order-2').html : ''}
          ${this.data.caption ? caption.html : ''}
        </article>
      `
    }


  }
}
